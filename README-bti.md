## bti360
# web.xml
	<?xml version="1.0" encoding="UTF-8"?>
	<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns="http://java.sun.com/xml/ns/javaee"
		xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
		xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
		id="WebApp_ID" version="2.5">
		<display-name>SpringDictionaryService</display-name>
		<servlet>
			<servlet-name>bti</servlet-name>
			<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
			<load-on-startup>1</load-on-startup>
		</servlet>
		<servlet-mapping>
			<servlet-name>bti</servlet-name>
			<url-pattern>/bti/*</url-pattern>
		</servlet-mapping>
		<welcome-file-list>
			<welcome-file>index.html</welcome-file>
		</welcome-file-list>
	</web-app>
	
#bti-servlet.xml
	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xmlns:mvc="http://www.springframework.org/schema/mvc"
		xmlns:context="http://www.springframework.org/schema/context"
		xmlns:p="http://www.springframework.org/schema/p"
		xsi:schemaLocation="
			http://www.springframework.org/schema/beans 
			http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
			http://www.springframework.org/schema/context 
			http://www.springframework.org/schema/context/spring-context-3.0.xsd
			http://www.springframework.org/schema/mvc 
			http://www.springframework.org/schema/mvc/spring-mvc-3.0.xsd">
	
		<context:component-scan base-package="org.bti" />
	
		<!-- Configures the @Controller programming model -->
		<mvc:annotation-driven />
	
		<mvc:view-controller path="/" view-name="index" />
	
	</beans>
	
#model Word

	package org.bti.ws.model;
	
	import javax.xml.bind.annotation.XmlRootElement;
	
	/**
	 * A simple word object that contains a name and some definitions.
	 */
	@XmlRootElement
	public class Word {
		private Integer id;
	    private String name;
	    private String definition;
		public Word() {
			
		}
	    public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDefinition() {
			return definition;
		}
		// ===============
		public Word setDefinition(String definition) {
			this.definition = definition;
			return this;
		}
		public Word(String name, String definition) {
			//this.id = id;
			this.name = name;
			this.definition = definition;
		}
		public Word(String name) {
	        this.name = name;
	    }
	//	@Override
	//	public String toString() {
	//		return "Word [id=" + id + ", name=" + name + ", definition=" + definition + "]";
	//	}
		@Override
		public String toString() {
	    	StringBuilder sb = new StringBuilder();
	    	sb.append("Name: ");
	    	sb.append(name);
	    	sb.append(" Definition: ");
	    	sb.append(definition);
	    	return sb.toString();
		}
	    
	}
	
#model ExtWrapper
	
	package org.bti.ws.model;
	
	import javax.xml.bind.annotation.XmlElement;
	import javax.xml.bind.annotation.XmlRootElement;
	
	@XmlRootElement(name="root")
	public class ExtWrapper {
	
		private static final long serialVersionUID = 1L;
	
		@XmlElement(name="data")
		Word[] data;
		
		@XmlElement(name="success")
		boolean success = true;
	
		public ExtWrapper() {}
	
		public Word[] getData() {
			return data;
		}
		
		public boolean isSuccess() {
			return success;
		}
	
		public ExtWrapper(Word[] data) {
			this.data = data;
		}
	}

#model ExtModelMap 

	package org.bti.ws.model;
	
	import org.springframework.ui.ModelMap;
	
	public class ExtModelMap extends ModelMap{
		
		private static final long serialVersionUID = 1L;
		private static final String SUCCESS = "success";
		private static final String ROOT = "data";
	
		public ExtModelMap(){
			this.addAttribute(SUCCESS, true);
		}
		
		public ExtModelMap(Object data){
			this();
			this.addAttribute(ROOT, data);
		}
	}

#persistence interface DictionaryDao

	package org.bti.ws.persistence;
	
	import java.util.List;
	
	import org.bti.ws.model.Word;
	
	public interface DictionaryDao {
		
		void putWord(Word word);
		Word getWord(int word);
		void removeWord(int word);
		List<Word> getWords(); 
	
	}

#persistence DictionaryDaoImpl
	
	package org.bti.ws.persistence;
	
	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;
	
	import org.bti.ws.model.Word;
	import org.springframework.stereotype.Repository;
	
	@Repository
	public class DictionaryDaoImpl implements DictionaryDao{
	
		private Map<Integer, Word> map = new HashMap<Integer, Word>();
		private int ctr = 0;
		
		public DictionaryDaoImpl() {
			putWord(new Word("set", "A collection that can only contain unique elements"));
			putWord(new Word("list", "A collection that can only contain unique elements"));
		}
		
		@Override
		public void putWord(Word word) {
			Integer id = word.getId();
			if(id == null) {
				id = new Integer(ctr++);
				word.setId(id);
			}
			map.put(id, word);
		}
	
		@Override
		public Word getWord(int id) {
			return map.get(id);
		}
		
		@Override
		public void removeWord(int id) {
			map.remove(id);
		}
	
		@Override
		public List<Word> getWords() {
			List<Word> wordList = new ArrayList<Word>();
			for(Map.Entry<Integer, Word> entry : map.entrySet()) {
				wordList.add(entry.getValue());
			}
			return wordList;
		}
	
	}

#controller DictionaryController

	package org.bti.ws.controller;
	
	import java.util.List;
	
	import javax.servlet.http.HttpServletResponse;
	
	import org.bti.ws.model.ExtModelMap;
	import org.bti.ws.model.ExtWrapper;
	import org.bti.ws.model.Word;
	import org.bti.ws.persistence.DictionaryDao;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.http.HttpStatus;
	import org.springframework.stereotype.Controller;
	import org.springframework.web.bind.annotation.ExceptionHandler;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.ResponseBody;
	import org.springframework.web.bind.annotation.ResponseStatus;
	import org.springframework.web.context.request.WebRequest;
	
	@Controller
	@RequestMapping("/dictionary")
	public class DictionaryController {
	
		@Autowired
		DictionaryDao dictionaryDao;
	
		@ResponseBody
		@RequestMapping(method = RequestMethod.GET)
		ExtWrapper get() {
			List<Word> l = dictionaryDao.getWords();
			Word[] words = new Word[l.size()];
			l.toArray(words);
			return new ExtWrapper(words);
		}
	
		@ResponseBody
		@RequestMapping(value = "/{id}", method = RequestMethod.GET)
		ExtModelMap get(@PathVariable("id") int id) {
			Word w = dictionaryDao.getWord(id);
			if (w == null) {
				throw new NotFoundException();
			}
			return new ExtModelMap(w);
		}
	
		@ResponseBody
		@RequestMapping(method = RequestMethod.POST)
		ExtModelMap post(@RequestBody Word word, WebRequest req,
				HttpServletResponse resp) {
			dictionaryDao.putWord(word);
			return new ExtModelMap(word);
		}
	
		@ResponseBody
		@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
		ExtModelMap put(@PathVariable("id") int id, @RequestBody Word word) {
			dictionaryDao.putWord(word);
			return new ExtModelMap(word);
		}
	
		@ResponseBody
		@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
		ExtModelMap remove(@PathVariable("id") int id) {
			dictionaryDao.removeWord(id);
			return new ExtModelMap();
		}
	
		@ExceptionHandler(NotFoundException.class)
		@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Word not found")
		void handleNotFound(NotFoundException exc) {
		}
	
		class NotFoundException extends RuntimeException {
			private static final long serialVersionUID = 1L;
		}
	}
		
		
		