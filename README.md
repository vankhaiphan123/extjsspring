#Crud Operation in ExtJs
	https://www.mindstick.com/articles/12244/crud-operation-in-extjs

#Controller

	using _20012017.Models;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;
	using System.Web.Mvc;
	using System.Data.SqlClient;
	namespace _20012017.Controllers
	{
	    public class UserController : Controller
	    {
	        //
	        // GET: /User/
	        static string message = string.Empty;   //define string type static variable 
	        static bool success = false;   //define bool type static variable
	        public ActionResult Index()
	        {
	            return View();
	        }
	        [HttpPost]
	        public JsonResult Save(Employee model) //define Save function thats call from controller.Its return type is JsonResult 
	        {
	            try    //all statement written in try block thats may be causes abnormal conditions(exception)
	            {
	                using (harsh_dbEntities db = new harsh_dbEntities()) //harsh_dbEntities is object thats add on ADO.NET Entity Data Model
	                {
	                    db.EmployeeMaster.Add(new EmployeeMaster  //EmployeeMaster is table name in database and Add function call for store data into database
	                    {
	                        Address = model.Address,  //store data into database table column Address
	                        ContactNo = model.PhoneNo,  //store data into database table column ContactNo
	                        DOB = model.DOB,   //store data into database table column DOB
	                        FatherName = model.FatherName,   //store data into database table column FatherName
	                        Name = model.Name   //store data into database table column Name
	                    });
	                    db.SaveChanges();  //call function SaveChanges() associated with database object db for save change into database
	                    success = true;
	                    message = 'Record saved successfully';
	                }
	            }
	            catch (Exception ex)
	            {
	                success = false;
	                message = ex.Message;
	            }
	            return Json(  //return response in the form of Json
	                new
	                {
	                    success,
	                    message
	                }, JsonRequestBehavior.AllowGet);
	        }
	        public JsonResult Show()  //define Show function 
	        {
	            try
	            {
	                using (harsh_dbEntities db = new harsh_dbEntities())//harsh_dbEntities is object thats add on ADO.NET Entity Data Model
	                {
	                    var data = db.EmployeeMaster.ToList();  //get all data from database and assign in data variable
	                    success = true;
	                    return Json(
	                                new
	                                {
	                                    data,
	                                    success
	                                }, JsonRequestBehavior.AllowGet);
	                }
	            }
	            catch (Exception ex)
	            {
	                success = false;
	                message = ex.Message;
	                return Json(
	                                new
	                                {
	                                    success,
	                                    message
	                                }, JsonRequestBehavior.AllowGet);
	            }
	        }
	        [HttpPost]
	        public JsonResult Update(Employee model)
	        {
	            try
	            {
	                using (harsh_dbEntities db = new harsh_dbEntities())
	                {
	                    //int id = Convert.ToInt32(model.ID);
	                    var data = db.EmployeeMaster.FirstOrDefault(x => x.ID == model.ID);  //FirstOrDefault function to select single record for get ID and check its equal to id return 
	                    data.Name = model.Name;  //update data into database column Name
	                    data.ContactNo = model.PhoneNo;   //update data into database column ContactNo
	                    data.FatherName = model.FatherName;   //update data into database column Address
	                    data.Address = model.Address;  //update data into database column Address
	                    data.DOB = model.DOB;   //update data into database column DOB
	                    //db.EmployeeMaster.Attach(data);
	                    db.SaveChanges();  //call SaveChanges() function
	                    success = true;
	                    message = 'Record updated successfully';
	                }
	            }
	            catch (Exception ex)
	            {
	                success = false;
	                message = ex.Message;
	            }
	            return Json(
	                new
	                {
	                    success,
	                    message
	                }, JsonRequestBehavior.AllowGet);
	        }
	        [HttpPost]
	        public JsonResult Delete(Employee model)
	        {
	            try
	            {
	                using (harsh_dbEntities dbs = new harsh_dbEntities())
	                {
	                    var data = dbs.EmployeeMaster.Where(a => a.ID == model.ID).FirstOrDefault();//when we select multiple record then write .ToList() instead of FirstOrDefault() 
	                    if (data != null)  //check condition if data is not null false 
	                    {
	                        dbs.EmployeeMaster.Remove(data);  //delete record in database,call Remove function and pass object of selected column
	                        dbs.SaveChanges();  //call SaveChanges() function
	                        success = true;
	                        message = 'Record deleted successfully';
	                    }
	                    else
	                    {
	                        success = false;
	                        message = 'Please select record on grid';
	                    }
	                }
	            }
	            catch (Exception ex)
	            {
	                success = false;
	                message = ex.Message;
	            }
	            return Json(  //return Json data
	                new
	                {
	                    success,
	                    message
	                }, JsonRequestBehavior.AllowGet);
	        }
	    }
	}