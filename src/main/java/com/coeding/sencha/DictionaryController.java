package com.coeding.sencha;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import com.coeding.sencha.dao.DictionaryDao;
import com.coeding.sencha.model.ExtModelMap;
import com.coeding.sencha.model.ExtWrapper;
import com.coeding.sencha.model.Word;

@Controller
@RequestMapping("/dictionary")
public class DictionaryController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	DictionaryDao dictionaryDao;

	@RequestMapping(value = "/")
	public String extjsListCustomer(Locale locale) {
		logger.info("Default Home REST page. The client locale is {}.", locale);
		return "dictionary";	
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody ExtWrapper get() {
		List<Word> list = dictionaryDao.getWords(); 	System.out.println("list: "+ list);
		Word[] words = new Word[list.size()];	 System.out.println("words: "+ words);
		list.toArray(words);	System.out.println("list: "+ list);
		return new ExtWrapper(words);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody ExtModelMap get(@PathVariable("id") int id) {
		Word word = dictionaryDao.getWord(id);
		if(word == null) {
			throw new NotFoundException();
		}
		return new ExtModelMap(word);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody ExtModelMap post(@RequestBody Word word, WebRequest req, HttpServletResponse resp) {
		dictionaryDao.putWord(word);
		return new ExtModelMap(word);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseBody ExtModelMap put(@PathVariable("id") int id, @RequestBody Word word) {
		dictionaryDao.putWord(word);
		return new ExtModelMap(word);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody ExtModelMap remove(@PathVariable("id") int id) {
		dictionaryDao.removeWord(id);
		return new ExtModelMap();
	}
	
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Word not found")
	void handleNotFound(NotFoundException exc) {
	}
	
	class NotFoundException extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}
}
