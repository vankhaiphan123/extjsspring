package com.coeding.sencha;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
//	
//	  @RequestMapping(value = "/update", method = RequestMethod.POST)
//	    public void updateUser(@RequestBody List<User> inUsers) throws Exception    {
//	        for (User user : inUsers) {
//	            User localUser = users.get(user.getId());
//	            if (localUser == null) {
//	                throw new RuntimeException("Invalid User");
//	            }
//
//	            // save changes to local user
//	            localUser.setName(user.getName());
//	            localUser.setEmail(user.getEmail());
//	        }
//	    }

	
}
