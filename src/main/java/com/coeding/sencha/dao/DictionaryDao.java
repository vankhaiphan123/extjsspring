package com.coeding.sencha.dao;

import java.util.List;

import com.coeding.sencha.model.Word;

public interface DictionaryDao {
	
	void putWord(Word word);
	Word getWord(int word);
	void removeWord(int word);
	List<Word> getWords(); 

}