package com.coeding.sencha.model;

import java.util.Date;

public class Employee {
	private int Id;
	private String Name;
	private String FatherName;
	private Date DOB;
	private String Address;
	private String PhoneNo;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getFatherName() {
		return FatherName;
	}
	public void setFatherName(String fatherName) {
		FatherName = fatherName;
	}
	public Date getDOB() {
		return DOB;
	}
	public void setDOB(Date dOB) {
		DOB = dOB;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getPhoneNo() {
		return PhoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}
	public Employee(int id, String name, String fatherName, Date dOB, String address, String phoneNo) {
		Id = id;
		Name = name;
		FatherName = fatherName;
		DOB = dOB;
		Address = address;
		PhoneNo = phoneNo;
	}
	public Employee() {

	}
	@Override
	public String toString() {
		return "Employee [Id=" + Id + ", Name=" + Name + ", FatherName=" + FatherName + ", DOB=" + DOB + ", Address="
				+ Address + ", PhoneNo=" + PhoneNo + "]";
	}
}
