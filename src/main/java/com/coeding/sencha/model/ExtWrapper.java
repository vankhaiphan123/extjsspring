package com.coeding.sencha.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="root")
public class ExtWrapper {

	private static final long serialVersionUID = 1L;

	@XmlElement(name="data")
	Word[] data;
	
	@XmlElement(name="success")
	boolean success = true;

	public ExtWrapper() {}

	public Word[] getData() {
		return data;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public ExtWrapper(Word[] data) {
		this.data = data;
	}
}
