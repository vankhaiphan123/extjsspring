package com.coeding.sencha.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A simple word object that contains a name and some definitions.
 */
@XmlRootElement
public class Word {
	private Integer id;
    private String name;
    private String definition;
	public Word() {
		
	}
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDefinition() {
		return definition;
	}
	// ===============
	public Word setDefinition(String definition) {
		this.definition = definition;
		return this;
	}
	public Word(String name, String definition) {
		//this.id = id;
		this.name = name;
		this.definition = definition;
	}
	public Word(String name) {
        this.name = name;
    }
//	@Override
//	public String toString() {
//		return "Word [id=" + id + ", name=" + name + ", definition=" + definition + "]";
//	}
	@Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("Name: ");
    	sb.append(name);
    	sb.append(" - Definition: ");
    	sb.append(definition);
    	return sb.toString();
	}
    
}
