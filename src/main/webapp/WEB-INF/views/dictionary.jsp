<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<link REL="SHORTCUT ICON" HREF="${pageContext.request.contextPath }/resources/images/bti.jpg">
		<title>Hello BTIers</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resources/css/ext-all.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resources/lib/ext/ux/css/RowEditor.css" />
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/lib/ext/ext-base-debug.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/lib/ext/ext-all-debug.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/lib/ext/ux/RowEditor.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/writer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/dictionaryGrid.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/restTest.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resources/js/main.js"></script>
	</head>
	
	<body>
		<div id="header"></div>
	</body>
</html>