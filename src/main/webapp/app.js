//Ext.application({		
//	 name : 'sencha',
//	 
//	 launch : function() {
//		 Ext.create('widget.panel', {
//			 renderTo: Ext.getBody(),
//			 title: 'Panel'
//		 });
//	 }
//});

Ext.application({
    extend: 'Ext.app.Application',   //extend app.Application class
    name: 'sencha',
    
    requires: [
    	'Ext.container.Viewport', 
    	'sencha.view.TabView'
    ],   // Define your requirements thats you call in this file
    
    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'anchor',
            items: [{
                xtype: 'TabView'  //Define your type of component,Its define in alias of TabView class
            }]
        });
    }
});