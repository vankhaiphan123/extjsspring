Ext.define('sencha.store.TabStore',  //define TabStore class
{
    extend: 'Ext.data.Store',   //extend data.Store
    model: 'sencha.model.TabModel',   //use model and give its path
    storeId: 'tabStore',
    autoLoad: true,
    proxy: {  
        type: 'ajax',
        url: './User/Show',   //give path of Show method in MVC controller
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});