Ext.define('MOST.view.document.DeliveryOrderController', {
	extend: 'MOST.view.foundation.BaseViewController',

	requires: [
	],

	alias: 'controller.deliveryorder',
	
	MAX_PERIOD_DAY : 14,
	MAX_DATE_ALLOW : 14,	// MAX PERIOD DATE (a week)
	authority: '',
	
	onLoad: function(){
		var me = this;
		var refs = me.getReferences();
		me.setDateInDays("ctlFromDt",-7);
		
		if(MOST.config.Token.getUserType() === CONSTANTS.USER_TYPE_EXTERNAL){
			if(me.existsPatnerType('SHA')){
				me.authority = 'SHA';
			} 
			if(me.existsPatnerType('FWD')){
				me.authority = 'FWD';
				refs.ctlFwdAgent.setValue(MOST.config.Token.getPtnrCode());
			} 
			if(me.existsPatnerType('SHA') && me.existsPatnerType('FWD')){
				me.authority = 'BH';
			}
		}
	},
	
	
	/**
	 * =========================================================================================================================
	 * EVENT HANDLER START
	 */
	// Search Event Handler
	onSearch: function() {
		var me = this;
     	var refs = me.getReferences();
    	var store = me.getStore('listOfDeliveryOrder');
    	var params = me.getSearchCondition();
    	
    	if(params == null){
    		return;
    	}
    	
		store.load({
			params: params,
			callback: function(records, operation, success) {
				if (success) {
					if (records.length > 0) {
						var theMainModel = Ext.create('MOST.model.document.DeliveryOrder');
						theMainModel.data = records[0].data;
						me.getViewModel().setData({theJpvc:theMainModel});
					}else{
						MessageUtil.warning("deliveryOrder", "datanotfound_msg");
						return;
					}
				}
			}
		});
	},
	
	// Toolbar Save Button
	onDetailSave:function(){
		var me = this;
		var detailView = me.getDetailBizView();
		var refs = me.getReferences();
		var detailItem = me.getViewModel().get('theMain');
		var userAgency = MOST.config.Token.getAgencyCode();
		if(userAgency != refs.refTxtFA.value && MOST.config.Token.getUserType() != 'I'){
			MessageUtil.warning("deliveryOrder", "deliveryOrder_bl_notassign_msg");
			return;
		}
		
		
		if(detailView){
			var infoForm = detailView.down('form').getForm();
			if(StringUtil.isNullorEmpty(refs.ctlCommodity.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_Commodity_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.refTxtCargoType.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_cargo_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.ctlPartnerCodeMultiField.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_transporter_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.ctlFinalDes.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_final_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.ctlTypeOfPackage.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_pakage_Empty_msg");
				return;
			}
			var date = refs.ctlETAFromDt.getValue();
			var dateString =  date==null?null:Ext.Date.format(date, MOST.config.Locale.getDefaultDateFormatWithNoSeconds());
			if(StringUtil.isNullorEmpty(dateString)){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_Expect_Arrival_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.ctlShipperCd.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_shipper_Empty_msg");
				return;
			}
			if(StringUtil.isNullorEmpty(refs.ctlConsigneeCd.getValue())){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_Consignee_Empty_msg");
				return;
			}
			if(infoForm.isValid()){
				me.checkValidation()
			} else {
				MessageUtil.mandatoryFieldInValid();
			}
		}
	},
	
	// Server Save Process
	saveProcess:function(){
		var me = this;
		var refs = me.getReferences();
		
		var store = me.getStore('listOfDeliveryOrder');
		var detailItem = me.getViewModel().get('theMain');
		var detailView = me.getDetailBizView();
		var recvData = detailView.items.get(0).recvData;
		var expectLorryArrv = Ext.Date.format(refs.ctlETAFromDt.getValue(), MOST.config.Locale.getDefaultDateFormatWithNoSeconds());
		var doNo = detailItem.get('dono');
		detailItem.data.estDt = expectLorryArrv;
		
		if(detailItem.dirty){
			var proxy = detailItem.getProxy();
			proxy.url = store.getProxy().url; // You can set it as store Proxy Url, or you can put another URL.

			detailItem.save({
				success : function(record,operation){
					detailItem.set("version", detailItem.get('newVersion'));
					detailItem.commit();
					detailItem.data = record.data;
					//me.updateRecord(recvData, detailItem); 
					MessageUtil.saveSuccess(); // Success Message
				}
			});
		}
	},
	
	checkValidation: function(){
		var me = this;
		var detailView = me.getDetailBizView();
		var detailItem = me.getViewModel().get('theMain');
		
		var cgOpList = me.getStore('cgOpList');
		
		cgOpList.load({
			params: {
				vslCallId : detailItem.data.vslCallId,
				blNo : detailItem.data.blno,
				searchType : "cgOp"
			},
			callback: function(records, operation, success) {
				if (success) {
					if (records.length > 0) {
						for (var i = 0; i < records.length; i++){
							if( records[i].statCd != 'RS'){
								MessageUtil.warning("deliveryOrder", "deliveryOrder_update_operated_msg");
								return;
							}
						}
					}
					
					if (!me.weightCheck()){
						return;
					}
					
					if (!me.calcM3()){
						return;
					}
					
					if (!me.calcQty()){
						return;
					}
					
					me.saveProcess();
				}
			}
		});
		
	},
	
	weightCheck: function(){
		var me = this;
		var refs = me.getReferences();
		
		var dMt = 0;
		var iMt = 0;
		
		var isEmptyDLrryMt = (refs.ctlDLrryMt.value == 0);
		var isEmptyDWagonMt = (refs.ctlDWagonMt.value == 0);
		var isEmptyDCnvyMt = (refs.ctlDCnvyMt.value == 0);
		var isEmptyDPplnMt = (refs.ctlDPplnMt.value == 0);
		
		if (refs.ctlModeOper.value != "") {
			if ((refs.ctlDLrryMt.editable == true && isEmptyDLrryMt)	
				|| (refs.ctlDWagonMt.editable == true && isEmptyDWagonMt)
				|| (refs.ctlDCnvyMt.editable == true && isEmptyDCnvyMt)
				|| (refs.ctlDPplnMt.editable == true && isEmptyDPplnMt)) {
				MessageUtil.warning("deliveryOrder", "deliveryOrder_update_mt_msg");
	    		return false;
			} 
		}
		
		var isEmptyILrryMt = (refs.ctlILrryMt.value == 0);
		var isEmptyIWagonMt = (refs.ctlIWagonMt.value == 0);
		
		if (refs.ctlIModeOper.value != "") {
			if ((refs.ctlILrryMt.editable == true && isEmptyILrryMt) || (refs.ctlIWagonMt.editable == true && isEmptyIWagonMt)) {
				MessageUtil.warning("deliveryOrder", "deliveryOrder_update_mt_msg");
	    		return false;
			} 
		}
		
		if (refs.ctlDDmt.value > 0) {
			dMt = Number(refs.ctlDDmt.value);
		}
		
		if (refs.ctlImt.value > 0) {
			iMt = Number(refs.ctlImt.value);
		}
		
		var sumMt = dMt + iMt;
		
		if (refs.ctlDocMt.value.length > 0) {
			var docMt = Number(refs.ctlDocMt.value);
			var inOpeMode = refs.ctlIModeOper.getValue();
			var dOpeMode = refs.ctlModeOper.getValue();
			if (docMt != sumMt ){
					MessageUtil.warning("deliveryOrder", "deliveryOrder_update_weight_msg");
		    		return false;
			}
		}
		
		return true;
	},
	
	calcM3: function(){
		var me = this;
		var refs = me.getReferences();
		
		var dM3 = 0;
		var iM3 = 0;
		
		if (refs.ctlDM3.value > 0) {
			dM3 = Number(refs.ctlDM3.value);
		}
		
		if (refs.ctlIm3.value > 0) {
			iM3 = Number(refs.ctlIm3.value);
		}
			
		var sumM3 = dM3 + iM3;
		
		if (refs.ctlVol.value.length > 0) {
			var docM3 = refs.ctlVol.value;
			if (docM3 != sumM3) {
				MessageUtil.warning("deliveryOrder", "deliveryOrder_update_measurement_msg");
				return false;
			}
		}
		
		return true;
	},
	
	calcQty: function(){
		var me = this;
		var refs = me.getReferences();
		
		var dQty = 0;
		var iQty = 0;
		
		if (refs.ctlDQty.value > 0) {
			dQty = Number(refs.ctlDQty.value);
		}
		
		if (refs.ctlIQty.value > 0) {
			iQty = Number(refs.ctlIQty.value);
		}
			
		var sumQty = dQty + iQty;
		
		if (refs.ctlPkgQty.value.length > 0) {
			var docQty = Number(refs.ctlPkgQty.value);
			if (docQty != sumQty){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_update_quantity_msg");
				return false;
			}
		}
		
		return true;
	},
	
	onRemove: function(){
		var me = this;
		var store = me.getStore('listOfDeliveryOrder');
		var grid = me.lookupReference('refListOfDeliveryOrderGrid');
		var selection = grid.getSelection() == null ? null : grid.getSelection()[0];
		
		if(selection == null) {
			MessageUtil.warning("deliveryOrder", "deliveryOrder_select_msg");
    		return;
		}else{
			
			if(selection.data.gpYn != null && selection.data.gpYn == 'Y'){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_delete_gp_msg");
	    		return;
			}
			
			if(selection.data.dono == null || selection.data.dono == ''){
				MessageUtil.warning("deliveryOrder", "fwaNomi_nodo_detail_msg");
				return;
			}
			
			MessageUtil.question('remove', 'deliveryOrder_delete_msg', null,
				function(button){
					if (button === 'ok') {
						selection.data.crudFlag = 'D';
						selection.data.dono = '';
						var proxy = selection.getProxy();
						proxy.url = store.getProxy().url; // You can set it as store Proxy Url, or you can put another URL.

						selection.save({
							success : function(records,success){
								//me.onDetailLoad();
								me.onSearch();
								MessageUtil.saveSuccess(); // Success Message
							}
						});
					}
				}
			);
			
			
		}
	},
	
	onDblClick: function() {
		var me = this;
		var grid = me.lookupReference('refListOfDeliveryOrderGrid');

		var selection = grid.getSelection() == null ? null : grid.getSelection()[0];
		
		if(selection == null){
			return;
		}else{
			if(selection.get('cgtpcd') == '' || selection.get('cgtpcd') == null){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_cargotype_empty_msg");
	    		return;
			}
		}
		
		me.openDetailPopup(selection);
	},
	
	// Date Change Event
	onDateChange:function( control, newValue, oldValue, eOpts ) {
		var me = this;
		var refs = me.getReferences();

		if(control == refs.ctlFromDt){
			me.setDateInDaysByDate("ctlToDt", me.MAX_PERIOD_DAY, control.getValue());
		} else {
			me.setDateInDaysByDate("ctlFromDt", -me.MAX_PERIOD_DAY, control.getValue());
		}
	},
	
	/**
	 * EVENT HANDLER END
	 * =========================================================================================================================
	 */
	
	/**
	 * =========================================================================================================================
	 * GENERAL METHOD START
	 */
	// Search Condition
	getSearchCondition : function(){
		var me = this;
     	var refs = me.getReferences();
     	var jpvcNo = refs.ctlJpvc.getValue();
     	var fwrd = refs.ctlFwdAgent.getValue();;
     	var delvtpcd = refs.ctlDeliveryMode.getValue();
     	var dateCondition = null;
     	var etaFrom = '';
     	var etaTo = '';
     	
     	if((jpvcNo == '' || jpvcNo == null) 
     			&& (fwrd == '' || fwrd == null) 
     			&& (delvtpcd == '' || delvtpcd == null) 
     			&& (refs.ctlFromDt.getValue() == '' || refs.ctlFromDt.getValue() == null)
     			&& (refs.ctlToDt.getValue() == '' || refs.ctlToDt.getValue() == null)){
     		MessageUtil.warning("deliveryOrder", "deliveryOrder_search_null_msg");
    		return;
     	}
     	
     	if(refs.ctlFromDt.getValue() != null && refs.ctlToDt.getValue() != null){
     		 dateCondition = me.checkPeriodDate("ctlFromDt", "ctlToDt", me.MAX_DATE_ALLOW, true);
     		etaFrom = dateCondition.fromDtString;
    		etaTo = dateCondition.toDtString;
     	}
     	
     	var searchType = 'list';
    	var params = {
    		vslCallId : jpvcNo,
    		searchType: searchType,
    		etaFrom: etaFrom,
    		etaTo: etaTo,
    		fwrd: fwrd,
    		delvtpcd: delvtpcd,
    		authority: me.authority,
    		ptnrcd: MOST.config.Token.getPtnrCode()
		};
    	
    	return params;
	},
	
	afterSetCodePopupData:function(xtype, targetControl, returnValue){
		var me = this;
		var refs = me.getReferences();
		if(targetControl == 'ctlJpvc'){ 
			if(returnValue){
				refs.ctlFromDt.setValue('');
				refs.ctlToDt.setValue('');
				
				me.onSearch();
			} 
		}else if(targetControl == 'ctlCommodity'){
			if(returnValue){
				refs.ctlCommodityNm.setValue(returnValue.codeName);
			} 
		}else if(targetControl === 'ctlShipperCd'){
			
			if(returnValue){ 
				//shipNote.set("shpr",returnValue.item.get("ptyCd"));
				
				refs.refTxtShprNm.setValue(returnValue.item.get("engPtyNm"));
				refs.refTxtSprAddr1.setValue(returnValue.item.get("addr1"));
				refs.refTxtSprAddr2.setValue(returnValue.item.get("addr2"));
				refs.refTxtSprAddr3.setValue(returnValue.item.get("addr3"));
				refs.refTxtSprAddr4.setValue(returnValue.item.get("addr4"));
			
			} else {
				refs.refTxtShprNm.setValue("");
				refs.refTxtSprAddr1.setValue("");
				refs.refTxtSprAddr2.setValue("");
				refs.refTxtSprAddr3.setValue("");
				refs.refTxtSprAddr4.setValue("");
			}
			
		}else if(targetControl === 'ctlConsigneeCd'){
			if(returnValue){ 
				//shipNote.set("shpr",returnValue.item.get("ptyCd"));
				
				refs.refTxtCnsNm.setValue(returnValue.item.get("engPtyNm"));
				refs.refTxtCnsAddr1.setValue(returnValue.item.get("addr1"));
				refs.refTxtCnsAddr2.setValue(returnValue.item.get("addr2"));
				refs.refTxtCnsAddr3.setValue(returnValue.item.get("addr3"));
				refs.refTxtCnsAddr4.setValue(returnValue.item.get("addr4"));
			
			} else {
				refs.refTxtCnsNm.setValue("");
				refs.refTxtCnsAddr1.setValue("");
				refs.refTxtCnsAddr2.setValue("");
				refs.refTxtCnsAddr3.setValue("");
				refs.refTxtCnsAddr4.setValue("");
			}
		}else if(targetControl === 'ctlFinalDes'){
			
			if(returnValue){
				refs.ctlFinalDes.setValue(returnValue.code);
			} 
		}
		
	},
	/*
	onDownloadPDF : function(){
		var me = this;
		var refs = me.getReferences();
		var downloadPDF = me.getStore('generatePDFDeliveryOrder');
		var params = me.getSearchCondition();
		var jpvcNo = refs.ctlJpvc.getValue(); 
		
		if(jpvcNo == '' || jpvcNo == null){
			MessageUtil.warning("deliveryOrder", "deliveryOrder_search_null_msg");
    		return;
		}
		else{
			downloadPDF.load({
				params: params,
				callback: function(records, operation, success) {
					if (success) {
						var content = records[0].data.content.replace(/&lt;/gi,'<').replace(/&gt;/gi,'>'); 
						Ext.exporter.File.saveBinaryAs(content, records[0].data.fileName);
					}
				}
			})
		}
			
	},
	
	onPreviewPDF:function(){
		var me = this;
		var refs = me.getReferences();
		var previewPDF = me.getStore('generatePDFDeliveryOrder');
		var params = me.getSearchCondition();
		var jpvcNo = refs.ctlJpvc.getValue(); 
		
		if(jpvcNo == '' || jpvcNo == null){
			MessageUtil.warning("deliveryOrder", "deliveryOrder_search_null_msg");
    		return;
		}
		else{
			previewPDF.load({
				params: params,
				callback: function(records, operation, success) {
					if (success) {
						me.openPDFPreview (records, operation, success);
					}
				}
			})
		}
		
	},
	*/
	
	
	
	
	/**
	 * GENERAL METHOD END
	 * =========================================================================================================================
	 */
	
	
	/**
	 * =========================================================================================================================
	 * DETAIL START
	 */
	// Detail Load
	onDetailLoad:function(){
		var me = this;
		var detailView = me.getDetailBizView();
		var refs = me.getReferences();
		//var infoForm = detailView.down('form').getForm();
		//infoForm.isValid(); // Mandatory to appear red for.
		//detailView.setPosition(-400, -300, true);
		
		//refs.refLblMode.setStyle('text-align: right; display: inline-block !important,vertical-align: middle !important');
		
		refs.ctlPartnerCodeMultiField.refs.ctlField.setEditable(true); // editable for partnercdformultifield
		me.setDetailComboBox();
		me.setDetailInitialize();
	},
	
	// Detail Initialize
	setDetailInitialize:function(){
		var me = this;
		var detailView = me.getDetailBizView();
		var recvData = detailView.items.get(0).recvData;
		//detailView.center();
		this.setDetailControl(recvData);
	},
	
	setDetailComboBox:function(){
		var me = this;
		var directTstpCombo = me.getViewModel().getStore('directTstpCombo');
		var indirectTstpCombo = me.getViewModel().getStore('indirectTstpCombo');
		
		//directTstpCombo.clearData();
		//indirectTstpCombo.clearData();
		
		//directTstpCombo.removeAll();
		//indirectTstpCombo.removeAll();
		
		directTstpCombo.load({
			params: {
				lcd : 'MT',
				mcd : 'TSPTTP'
			},
			callback: function(records, operation, success) {
				if (success) {
					if (records.length > 0) {
						indirectTstpCombo.clearData();
						indirectTstpCombo.insert(0, [{
							scdNm: 'Select',
							scd: ''
						}]);
						var j = 1;
						for (var i = 0; i < records.length; i++){
							if( records[i].data.scd.indexOf('C') < 0 && records[i].data.scd.indexOf('P') < 0){
								indirectTstpCombo.insert(j, [{
						        	  scdNm: records[i].data.scdNm,
						              scd: records[i].data.scd
						          }]);
								j++
							}
						}
						
					}
				}
			}
		});
		
	},
	
	// Detail Control Setting
	setDetailControl:function(recvData){
		var me = this;
		var refs = me.getReferences();
		
		var deliveryOrderDetail = me.getStore('deliveryOrderDetail');
	
		//added by Vin - 20190530 - Mantis 91309 begin
		if(recvData.get('delvTpCd') == "I"){
			//refs.refFsDirect.setDisabled(true);
			me.onFieldsetDisabled(refs.refFsDirect, true)
		}
		
		if(recvData.get('delvTpCd') == "D"){
			//refs.refFsInDirect.setDisabled(true);
			me.onFieldsetDisabled(refs.refFsInDirect, true)
		}
		//added by Vin - 20190530 - Mantis 91309 end
		
		deliveryOrderDetail.load({
			params: {
				vslCallId : recvData.data.vslCallId,
				blno : recvData.data.blno,
				searchType: 'detail'
			},
			callback: function(records, operation, success) {
				if (success) {
					if (records.length > 0) {
						// Main Detail
						var theMainModel = Ext.create('MOST.model.document.DeliveryOrder');
						theMainModel.phantom = false; // UPDATE
						theMainModel.data = records[0].data;
						theMainModel.data.estdt = records[0].get('estDt');
						
						var dRec = Ext.create('MOST.model.common.codes.DetailCode'); // record for direct
						dRec.data.cd = records[0].get('tspttpcd');
						dRec.data.scd = records[0].get('tspttpcd');
						me.onSelectModeOfOper(refs.ctlModeOper, dRec)
						
						var iRec = Ext.create('MOST.model.common.codes.DetailCode'); // record for indirect
						iRec.data.cd = records[0].get('itspttpcd');
						iRec.data.scd = records[0].get('itspttpcd');
						me.onSelectModeOfOper(refs.ctlIModeOper, iRec)
						
						
						
						if(Ext.isEmpty(refs.ctlDLrryMt.getValue())){
							refs.ctlDLrryMt.setValue(0)
						}
						
						if(Ext.isEmpty(refs.ctlDWagonMt.getValue())){
							refs.ctlDWagonMt.setValue(0)
						}
						
						if(Ext.isEmpty(refs.ctlDCnvyMt.getValue())){
							refs.ctlDCnvyMt.setValue(0)
						}
						
						if(Ext.isEmpty(refs.ctlILrryMt.getValue())){
							refs.ctlDPplnMt.setValue(0)
						}
						
						if(Ext.isEmpty(refs.ctlILrryMt.getValue())){
							refs.ctlILrryMt.setValue(0)
						}
						
						if(Ext.isEmpty(refs.ctlIWagonMt.getValue())){
							refs.ctlIWagonMt.setValue(0)
						}
						me.getViewModel().setData({theMain:theMainModel});
					}
				}
			}
		});
		
	},
	
	// Commodity POPUP
	openCommodityPopup:function(){
		var me = this;
		var params = {
			searchType : 'CMDT'
		};
		me.openCodePopup('popup-cmmcdpopup', 'ctlCommodity', params);
	},
	
	// Packate Type POPUP
	openTpOfPkgPopup:function(){
		var me = this;
		var params = {
			searchType: 'COMM', 	// CNTRY, CMDT, COMM, IMDG, PORT, VSCD, DLYCD, ONE_DG
			searchDivCd: 'PKGTP',	// EMAILTMP, PKGTP
			searchLcd: 'MT',		// MT
			searchCol1: ''
		};
		me.openCodePopup('popup-cmmcdpopup', 'ctlTypeOfPackage', params);
	},

	onSelectModeOfOper:function(ele, rec, idx){
		var me = this;
		var refs = me.getReferences();
		var controlName = ele.getReference();
	
		if(controlName == 'ctlModeOper'){
			//variables was referred from flex version of MPTS,
			var isCVLC = (rec.get('cd') === 'CV' || rec.get('cd') === 'LC');
			var isLPLWC = (rec.get('cd') === 'LP' || rec.get('cd') === 'LWC');
			var isLWPLWCP = (rec.get('cd') === 'LWP' || rec.get('cd') === 'LWCP');
			var isWCWP = (rec.get('cd') === 'WC' || rec.get('cd') === 'WP');
			
			if(refs.refTxtCargoType.getValue() == 'Break Bulk Cargo' && (isCVLC || isLPLWC || isLWPLWCP || isWCWP)){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_mode_operation_msg");
	    		return;
			}else{
				
				/*refs.ctlDDmt.setValue('0.0');
				refs.ctlDDmt.setEditable(false);
				refs.ctlDDmt.setDisabled(true);
				
				refs.ctlDLrryMt.setValue('0.0');
				refs.ctlDLrryMt.setEditable(false);
				refs.ctlDLrryMt.setDisabled(true);
				
				refs.ctlDWagonMt.setValue('0.0');
				refs.ctlDWagonMt.setEditable(false);
				refs.ctlDWagonMt.setDisabled(true);
				
				refs.ctlDCnvyMt.setValue('0.0');
				refs.ctlDCnvyMt.setEditable(false);
				refs.ctlDCnvyMt.setDisabled(true);
				
				refs.ctlDPplnMt.setValue('0.0');
				refs.ctlDPplnMt.setEditable(false);
				refs.ctlDPplnMt.setDisabled(true);*/
				
				if(rec.get('scd').indexOf('L') >= 0){
					if(Ext.isEmpty(refs.ctlDLrryMt.getValue())){
						refs.ctlDLrryMt.emptyText ='0';
						refs.ctlDLrryMt.setEditable(true);
						refs.ctlDLrryMt.setReadOnly(false);
					}else{
						refs.ctlDLrryMt.setEditable(true);
						refs.ctlDLrryMt.setReadOnly(false);
					}
				}else{
					refs.ctlDLrryMt.emptyText ='0';
					refs.ctlDLrryMt.setEditable(false);
					refs.ctlDLrryMt.setReadOnly(true);
				}
				
				if(rec.get('scd').indexOf('W') >= 0){
					if(Ext.isEmpty(refs.ctlDWagonMt.getValue())){
						refs.ctlDWagonMt.emptyText ='0';
						refs.ctlDWagonMt.setEditable(true);
						refs.ctlDWagonMt.setReadOnly(false);
					}else{
						refs.ctlDWagonMt.setEditable(true);
						refs.ctlDWagonMt.setReadOnly(false);
					}
					
				}else{
					refs.ctlDWagonMt.emptyText ='0';
					refs.ctlDWagonMt.setEditable(false);
					refs.ctlDWagonMt.setReadOnly(true);
				}
				
				if(rec.get('scd').indexOf('C') >= 0){
					if(Ext.isEmpty(refs.ctlDCnvyMt.getValue())){
						//refs.ctlDCnvyMt.setValue('0');
						refs.ctlDCnvyMt.emptyText ='0';
						refs.ctlDCnvyMt.setEditable(true);
						refs.ctlDCnvyMt.setReadOnly(false);
					}else{
						refs.ctlDCnvyMt.setEditable(true);
						refs.ctlDCnvyMt.setReadOnly(false);
					}
					
				}else{
					refs.ctlDCnvyMt.emptyText ='0';
					refs.ctlDCnvyMt.setEditable(false);
					refs.ctlDCnvyMt.setReadOnly(true);
				}
				
				if(rec.get('scd').indexOf('P') >= 0){
					if(Ext.isEmpty(refs.ctlDPplnMt.getValue())){
						//refs.ctlDPplnMt.setValue('0');
						refs.ctlDPplnMt.emptyText ='0';
						refs.ctlDPplnMt.setEditable(true);
						refs.ctlDPplnMt.setReadOnly(false);
					}else{
						refs.ctlDPplnMt.setEditable(true);
						refs.ctlDPplnMt.setReadOnly(false);
					}
					
				}else{
					refs.ctlDPplnMt.emptyText ='0';
					refs.ctlDPplnMt.setEditable(false);
					refs.ctlDPplnMt.setReadOnly(true);
				}
				
				if(rec.get('scd') == '' || rec.get('scd') == null){
					//refs.ctlDLrryMt.setValue('0');
					refs.ctlDLrryMt.emptyText ='0';
					refs.ctlDLrryMt.setEditable(false);
					refs.ctlDLrryMt.setReadOnly(true);
					
					//refs.ctlDWagonMt.setValue('0');
					refs.ctlDWagonMt.emptyText ='0';
					refs.ctlDWagonMt.setEditable(false);
					refs.ctlDWagonMt.setReadOnly(true);
					
					//refs.ctlDCnvyMt.setValue('0');
					refs.ctlDCnvyMt.emptyText ='0';
					refs.ctlDCnvyMt.setEditable(false);
					refs.ctlDCnvyMt.setReadOnly(true);
					
					//refs.ctlDPplnMt.setValue('0');
					refs.ctlDPplnMt.emptyText ='0';
					refs.ctlDPplnMt.setEditable(false);
					refs.ctlDPplnMt.setReadOnly(true);
				}
				
				if(rec.get('scd') == 'PL'){
					
					if(Ext.isEmpty(refs.ctlDLrryMt.getValue())){
						//refs.ctlDLrryMt.setValue('0');
						refs.ctlDLrryMt.emptyText ='0';
						refs.ctlDLrryMt.setEditable(false);
						refs.ctlDLrryMt.setReadOnly(true);
					}else{
						refs.ctlDLrryMt.setEditable(true);
						refs.ctlDLrryMt.setReadOnly(false);
					}
					
					if(Ext.isEmpty(refs.ctlDCnvyMt.getValue())){
						//refs.ctlDCnvyMt.setValue('0');
						refs.ctlDCnvyMt.emptyText ='0';
						refs.ctlDCnvyMt.setEditable(true);
						refs.ctlDCnvyMt.setReadOnly(false);
					}else{
						refs.ctlDCnvyMt.setEditable(true);
						refs.ctlDCnvyMt.setReadOnly(false);
					}
					
					
				}
				
				var arrMode = ['RR', 'SE', 'OH']
				if(arrMode.indexOf(rec.get('scd')) >= 0){
					refs.ctlDDmt.emptyText = '0';
					//refs.ctlImt.setValue('');
					refs.ctlDDmt.setEditable(true);
					refs.ctlDDmt.setReadOnly(false);
				}else{
					refs.ctlDDmt.setEditable(false);
					refs.ctlDDmt.setReadOnly(true);
				}
			}
		}
		
		if(controlName == 'ctlIModeOper'){
			var isCVLC = (rec.get('cd') === 'CV' || rec.get('cd') === 'LC');
			var isLPLWC = (rec.get('cd') === 'LP' || rec.get('cd') === 'LWC');
			var isLWPLWCP = (rec.get('cd') === 'LWP' || rec.get('cd') === 'LWCP');
			var isWCWP = (rec.get('cd') === 'WC' || rec.get('cd') === 'WP');
			
			if(refs.refTxtCargoType.getValue() == 'Break Bulk Cargo' && (isCVLC || isLPLWC || isLWPLWCP || isWCWP)){
				MessageUtil.warning("deliveryOrder", "deliveryOrder_mode_operation_msg");
	    		return;
			}else{
				
				if(rec.get('scd').indexOf('L') >= 0){
					if(Ext.isEmpty(refs.ctlILrryMt.getValue())){
						//refs.ctlILrryMt.setValue('0');
						refs.ctlILrryMt.emptyText = '0';
						refs.ctlILrryMt.setEditable(true);
						refs.ctlILrryMt.setReadOnly(false);
					}else{
						refs.ctlILrryMt.setEditable(true);
						refs.ctlILrryMt.setReadOnly(false);
					}
				}else{
					refs.ctlILrryMt.emptyText = '0';
					refs.ctlILrryMt.setEditable(false);
					refs.ctlILrryMt.setReadOnly(true);
				}
				
				
				if(rec.get('scd').indexOf('W') >= 0){
					if(Ext.isEmpty(refs.ctlIWagonMt.getValue())){
						//refs.ctlIWagonMt.setValue('0');
						refs.ctlIWagonMt.emptyText = '0';
						refs.ctlIWagonMt.setEditable(true);
						refs.ctlIWagonMt.setReadOnly(false);
					}else{
						refs.ctlIWagonMt.setEditable(true);
						refs.ctlIWagonMt.setReadOnly(false);
					}
				}else{
					refs.ctlIWagonMt.emptyText = '0';
					refs.ctlIWagonMt.setEditable(false);
					refs.ctlIWagonMt.setReadOnly(true);
				}
				var arrMode = ['RR', 'SE', 'OH']
				if(arrMode.indexOf(rec.get('scd')) >= 0){
					if(Ext.isEmpty(refs.ctlIWagonMt.getValue())){
						//refs.ctlImt.emptyText = '0';
						//refs.ctlImt.setValue('');
						refs.ctlImt.setEditable(true);
						refs.ctlImt.setReadOnly(false);
					}else{
						refs.ctlImt.setEditable(true);
						refs.ctlImt.setReadOnly(false);
					}
				}else{
					refs.ctlImt.setEditable(false);
					refs.ctlImt.setReadOnly(true);
				}
			}
		}
		
	},
	
	onIMtOver:function(field){
		var me = this;
		var refs = me.getReferences();
		var sumMt = 0;
		var arrMode = ['RR', 'SE', 'OH']
		var iMode = refs.ctlIModeOper.getValue();
		if(arrMode.indexOf(iMode)<0){
			if(Ext.isEmpty(refs.ctlILrryMt.getValue())){
			refs.ctlILrryMt.setValue(0)
		    }
		
			if(Ext.isEmpty(refs.ctlIWagonMt.getValue())){
				refs.ctlIWagonMt.setValue(0)
			}
		
			sumMt = parseFloat(refs.ctlILrryMt.getValue()) + parseFloat(refs.ctlIWagonMt.getValue());

			refs.ctlImt.setValue(sumMt);
		}
		//if(field.getValue() == "") field.setValue(0);

	},
	
	onDMtOver:function(field, event){
		var me = this;
		var refs = me.getReferences();
		var sumMt = 0;
		var arrMode = ['RR', 'SE', 'OH']
		var dMode = refs.ctlModeOper.getValue();
		if(arrMode.indexOf(dMode)<0){
			if(Ext.isEmpty(refs.ctlDLrryMt.getValue())){
				refs.ctlDLrryMt.setValue(0)
			}
			
			if(Ext.isEmpty(refs.ctlDWagonMt.getValue())){
				refs.ctlDWagonMt.setValue(0)
			}
			
			if(Ext.isEmpty(refs.ctlDCnvyMt.getValue())){
				refs.ctlDCnvyMt.setValue(0)
			}
			
			if(Ext.isEmpty(refs.ctlILrryMt.getValue())){
				refs.ctlDPplnMt.setValue(0)
			}
			
			sumMt = parseFloat(refs.ctlDLrryMt.getValue()) + parseFloat(refs.ctlDWagonMt.getValue()) + parseFloat(refs.ctlDCnvyMt.getValue()) + parseFloat(refs.ctlDPplnMt.getValue());
			
			sumMt = sumMt.toFixed(3);
			refs.ctlDDmt.setValue(sumMt);
		}
	
	},
	
	onCmdtBlur:function(field, event){
		var me = this;
		var refs = me.getReferences();
		var commonCodePopup = me.getStore('commonCodePopup');
		
		commonCodePopup.load({
			params:{
				searchType : 'CMDT',
				cd: field.getValue(),
				tyCd: 'CD'
			},
			callback:function(records, operation, success){
				if(success){
					if(records.length >= 1){
						refs.ctlCommodityNm.setValue(records[0].get('scdNm'));
						refs.ctlCommodity.setValue(records[0].get('scd'));
						return;
					}
					
					if(records.length == 0){
						MessageUtil.warning("deliveryOrder", "deliveryOrder_cmdt_exist_msg");
			    		return;
					}
				}
			}
		})
	},
	
	openFinalDesPopup:function(){
		var me = this;
		var params = {
			searchType : 'PORT'
		};
		me.openCodePopup('popup-cmmcdpopup', 'ctlFinalDes', params);
	},
	
	
	
	onControlBlur:function(field, event){
		var me = this;
		var refs = me.getReferences();
		var commonCodePopup = me.getStore('commonCodePopup');
		
		if(field.getValue() != '' && field.getValue() != null){
			if(field.getReference() == 'ctlFinalDes'){
				commonCodePopup.clearData();
				
				var params = {
					searchType:'PORT',
					cd: field.getValue().toUpperCase(),
					tyCd: 'CD'
				}
				
				commonCodePopup.load({
					params:params,
					callback:function(records, operation, success){
						if(success){
							if(records.length == 0){
								MessageUtil.warning("deliveryOrder", "deliveryOrder_cmdt_final_des_msg");
					    		return;
							}
							
							refs.ctlFinalDes.setValue(records[0].get('scd'));
						}
					}
				})
			}
			
			if(field.getReference() == 'ctlTypeOfPackage'){
				commonCodePopup.clearData();
				
				var params = {
					searchType: 'COMM', 	// CNTRY, CMDT, COMM, IMDG, PORT, VSCD, DLYCD, ONE_DG
					divCd: 'PKGTP',	// EMAILTMP, PKGTP
					lcd: 'MT',	// MT
					cd: field.getValue().toUpperCase(),
					tyCd: 'CD'
				}
				
				commonCodePopup.load({
					params:params,
					callback:function(records, operation, success){
						if(success){
							
							if(records.length == 0){
								MessageUtil.warning("deliveryOrder", "deliveryOrder_cmdt_typeofpkg_des_msg");
					    		return;
							}
							
							refs.ctlTypeOfPackage.setValue(records[0].get('scd'));
						}
					}
				})
			}
		}
		
	},
	

	onChangeNumber: function(e, text, prev){
		if((!/^[1-9][0-9]*$/gm.test(text)) && text != '0'){
			text = text.substr(1);
			e.setValue(text);
		}else if (text == '0'){
			e.setValue('0');
		}
		
	},
	/**
	 * =========================================================================================================================
	 * DETAIL END
	 */
	openExportTypePopup:function(){
		var me = this;
		var params = {
				initSearch: true
			};
		me.openCodePopup('popup-exporttypepopup','refBtnDownload', params);
	},
	
	onExport:function(){
		var me = this; 
		var refs = me.getReferences();
		var pdfStore = me.getStore('generatePDFDeliveryOrder');
		var theMain = me.getViewModel().getData().theJpvc;
		var ExportRadioChk = refs.refRadioReportType.getValue().rb;
		pdfStore.load({
			params:{
				vslCallId: theMain.get('vslCallId'),
				blNo: theMain.get('blNo'),
				delvtpcd: theMain.get('delvTpCd'),
				userId: MOST.config.Token.getUserId(),
				searchType:ExportRadioChk
			},
			callback: function(records, operation, success) {
				if (success) {
					var content = records[0].data.content.replace(/&lt;/gi,'<').replace(/&gt;/gi,'>'); 
					Ext.exporter.File.saveBinaryAs(content, records[0].data.fileName);
				}
			}
		})
		
		
	},
	
	onDetailPreview:function(){
		var me = this;
		var refs = me.getReferences();
		var pdfStore = me.getStore('generatePDFDeliveryOrder');
		var theMain = me.getViewModel().getData().theJpvc;
		
		if(!theMain){
			MessageUtil.warning("Delivery Order", "selectvesselid_msg");
			return;
		}
		
		pdfStore.load({
			params:{
				vslCallId: theMain.get('vslCallId'),
				blno: theMain.get('blno'),
				delvtpcd: theMain.get('delvTpCd'),
				userId: MOST.config.Token.getUserId(),
				searchType:'PDF'
			},
			callback: function(records, operation, success) {
				if (success) {
					me.openPDFPreview (records, operation, success);
				}
			}
		})
		
	},
	
	onDetailDownload(){
		var me = this;
		var refs = me.getReferences();
		
		var theMain = me.getViewModel().getData().theJpvc;
		if(!theMain){
			MessageUtil.warning("Delivery Order", "selectvesselid_msg");
			return;
		}
		
		var params = {
				initSearch: true
			};
		me.openCodePopup('popup-exporttypepopup','refBtnDownload', params);
	},
	
	onPreviewLoad:function(){
		var me = this;
		var refs = me.getReferences();
	},
	
	onDownloadCancel:function(ownWin){
		var me = this;
		var winAlias = "popupAliaspopup-exporttypepopup";
		var win = me.lookupReference(winAlias);
		
		win.close();
		
	},
	
	onDownloadExport:function(){
		var me = this; 
		var refs = me.getReferences();
		var pdfStore = me.getStore('generatePDFDeliveryOrder');
		var theMain = me.getViewModel().getData().theJpvc;
		var ExportRadioChk = refs.refRadioReportType.getValue().rb;
		pdfStore.load({
			params:{
				vslCallId: theMain.get('vslCallId'),
				blNo: theMain.get('blNo'),
				delvtpcd: theMain.get('delvTpCd'),
				userId: MOST.config.Token.getUserId(),
				searchType:ExportRadioChk
			},
			callback: function(records, operation, success) {
				if (success) {
					var content = records[0].data.content.replace(/&lt;/gi,'<').replace(/&gt;/gi,'>'); 
					Ext.exporter.File.saveBinaryAs(content, records[0].data.fileName);
				}
			}
		})
	},	
	
	/**
	 * HHT METHOD START
	 * =========================================================================================================================
	 */	
	
	onHHTLoad: function(){
		var me = this;
		var refs = me.getReferences();
//		me.setDateInDays("refFromDt",-me.MAX_DATE_ALLOW);
//		me.setDateInDays("refToDt");
		refs.refJpvcText.setRequired(true);
	},
	
	onSearchHHT: function(){
		var me = this;
		var refs = me.getReferences();
		var store = me.getStore('listOfDeliveryOrder');
    	var params = me.getSearchConditionHHT();
    	
    	if(params == null){
    		return;
    	}
    	
		store.load({
			params: params,
			callback: function(records, operation, success) {
				if (success) {
					if (records.length > 0) {
						
					}else{
						MessageUtil.warning("deliveryOrder", "datanotfound_msg");
						return;
					}
				}
			}
		});
	},
	
	getSearchConditionHHT : function(){
		var me = this;
     	var refs = me.getReferences();
     	var jpvcNo = refs.refJpvcText.getValue();
     	var blNo = refs.refblNo.getValue();
     	var dateCondition = null;
     	var etaFrom = '';
     	var etaTo = '';
     	if((jpvcNo == '' || jpvcNo == null) 
     		&& ((refs.refFromDt.getValue() == '' || refs.refFromDt.getValue() == null) || (refs.refToDt.getValue() == '' || refs.refToDt.getValue() == null))
     			){
     		MessageUtil.warning("deliveryOrder", "deliveryOrder_search_null_msg");
    		return;
     	}
     	
     	if(refs.refFromDt.getValue() != null && refs.refToDt.getValue() != null){
     		dateCondition = me.checkPeriodDate("refFromDt", "refToDt", me.MAX_DATE_ALLOW, true);
     		etaFrom = dateCondition.fromDtString;
    		etaTo = dateCondition.toDtString;
     	}
     	
     	var searchType = 'list';
    	var params = {
    		vslCallId : jpvcNo,
    		searchType: searchType,
    		blno: blNo,
    		etaFrom: etaFrom,
    		etaTo: etaTo,
		};
    	
    	return params;
	},
	
	// Grid Row Double
	onHHTDblClick: function() {
		var me = this; 
		var window = me.getView().up('window');
		
    	window.returnValue = me.getReturnData();
       	window.close();
	},
	
	// Returns the popup result.
	getReturnData:function(){
		var me = this;
		var selection;
		var grid = me.lookupReference('refDeliveryOrderHHTGrid');
		selection = grid.getSelection() == null ? null : grid.getSelection();
		var returnItem = {
			code : selection.data.dono,
			codeName : selection.data.dono,
			item : selection
		}
		
		return returnItem;
	},
	
	onSelectData : function(ref){
		var me = this; 
		var window = me.getView().up('window');
		if(ref.getReference() == 'refBtnSelectDOHHT'){//Select from JPVC:
			window.returnValue = me.getJPVCReturnDataHHT();
		}
		if(window.returnValue != null){
			window.close();	
		}
	},
	
	getJPVCReturnDataHHT : function() {
		var me = this;
		var refs = me.getReferences();
		var window = me.getView().up('window');
		
		var grid = me.lookupReference('refDeliveryOrderHHTGrid');
		selection = grid.getSelection() == null ? null : grid.getSelection();
		
		if(selection == null){
			MessageUtil.warning('Warning', 'tbl_sts_select');
			return null;
		}
		
		var returnItem = {
			code : selection.data.dono,
			codeName : selection.data.dono,
			item : selection
		}

    	window.returnValue = returnItem;
		if(window.returnValue != null){
			window.close();	
		}
	},
	
	 exportTo: function(btn) {
	    	var me = this;
	    	var refs = me.getReferences();
	    	
	        var cfg = Ext.merge({
	            title: 'Delivery Order',
	            fileName: 'Delivery_Order' + '.' + (btn.cfg.ext || btn.cfg.type)
	        }, btn.cfg);

	        var grid = refs.refListOfDeliveryOrderGrid;
	        grid.saveDocumentAs(cfg);
	    }
	/**
	 * HHT METHOD END
	 * =========================================================================================================================
	 */	
});