Ext.define('MOST.view.document.DeliveryOrderModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.deliveryorder',

	requires: [
		'Ext.data.proxy.Rest',
		'MOST.model.document.DeliveryOrder'
	],
	
	stores: {
		listOfDeliveryOrder: {
			model: 'MOST.model.document.DeliveryOrder',
			storeId: 'listOfDeliveryOrderStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/deliveryorder/list'
			}
		},
		
		deliveryOrderDetail: {
			model: 'MOST.model.document.DeliveryOrder',
			storeId: 'deliveryOrderDetail',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/deliveryorder/detail'
			}
		},
		
		cgOpList: {
			model: 'MOST.model.popup.CargoMaster',
			storeId: 'cgOpListStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/deliveryorder/cgOpList'
			}
		},
		
		directTstpCombo : {
			fields: ['scdNm','scd'],
			storeId: 'directTstpCombo',
			proxy: {
			   type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/commonCode/codeMasterList'
			},
			listeners: 
			{
				load: function(store, records) {
			          store.insert(0, [{
			        	  scdNm: 'Select',
			              scd: ''
			          }]);
			     }
			}
		},
		
		indirectTstpCombo : {
			fields: ['scdNm','scd'],
			storeId: 'indirectTstpComboStore',
		},
		
		delvModeCombo : {
			fields: ['name','code'],
			storeId: 'delvModeComboStore',
			data :  [{"code":"", "name":"Select"},
					{"code":"B", "name":"Both Direct/Indirect"},
					{"code":"I", "name":"Indirect"}
            ]
		},
		
		commonCodePopup: {
			model: 'MOST.model.popup.CMMCdPopup',
			storeId: 'commonCodePopupStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/commonCode/commonCodeList'
			}
		},
		
		generatePDFDeliveryOrder: {
			model: 'MOST.model.document.DeliveryOrder',
			storeId: 'generatePDFDeliveryOrderStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/deliveryorder/previewpdfdo'
			}
		},
	}
	
});