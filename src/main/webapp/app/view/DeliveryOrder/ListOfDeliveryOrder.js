Ext.define('MOST.view.document.ListOfDeliveryOrder', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.app-listofdeliveryorder',
	
	/* Marks these are required classes to be to loaded before loading this view */
	requires: [
		'MOST.view.document.DeliveryOrderController',
		'MOST.view.document.DeliveryOrderModel',
		'Ext.grid.plugin.RowEditing',
	    'Ext.grid.plugin.Exporter',
	    'Ext.grid.plugin.Clipboard',
		'Ext.grid.filters.Filters',
		'Ext.grid.selection.SpreadsheetModel'
	],
	
	detailViewAlias: 'app-deliveryorderdetail',
	
	controller: 'deliveryorder',
	
	/* View model of the view */
	viewModel: {
		type: 'deliveryorder'
	},
	
	listeners:{
		afterrender: 'onLoad'
	},
	
	lblSearch: {type: 'bundle', key: 'search'},
	lblEta: {type: 'bundle', key: 'eta'},
	lblJpvc: {type: 'bundle', key: 'jpvc'},
	lblForwardingAgent: {type: 'bundle', key: 'forwardingAgent'},

	
	btnRetrieve: {type: 'bundle', key: 'retrive'},
	btnDelete: {type: 'bundle', key: 'delete'},
	btnSearch: {type: 'bundle', key: 'search'},
	btnRefresh: {type: 'bundle', key: 'refresh'},
	btnRemove: {type: 'bundle', key: 'remove'},
	btnExportToPDF: {type: 'bundle', key: 'export'},
	btnPreviewToPDF: {type: 'bundle', key: 'preview'},
	btnDownload: {type: 'bundle', key: 'download'},
	
	layout : {type  : 'vbox', align : 'stretch'},

	initComponent: function() {
		var me = this;
		
		Ext.apply(me, {
			layout : {
				type: 'vbox',
				align: 'stretch'
			},
			margin: '5 5 0 0',
			
			items: [{
				xtype: 'fieldset',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				
				items: [{
			            xtype: 'container',
			            layout: {
			                type: 'vbox',
			                align: 'stretch'
			            },
			           
			            // =========== container ===========
			            items: [{
			                xtype: 'container',
			                layout: {
			                    type: 'hbox',
			                    align: 'stretch'
			                },
		                    defaults: {
		                        labelAlign: 'right',
		                        labelWidth: 80,
		                        margin: '0 5 0 0',
		                        editable: false
		                    },
		                    items:[
	                    	 {
	                        	xtype:'jpvcfield',
	        					reference:'ctlJpvc',
	        					fieldLabel: me.lblJpvc,
	        					labelWidth: 55,
	                            width:230,
	                            editable: true,
	        					emptyText: me.lblJpvc
	        				},
	        				{
	                        	xtype:'partnercdforgridfield',
	        					reference:'ctlFwdAgent',
	        					fieldLabel: me.lblForwardingAgent,
	        					labelWidth: 120,
	                            width:280,
	                            align : 'left',
	    	   					params:{
	    	   						searchPtyDivCd: 'FWD' // CNS, FWD, TRK
    	   						}
	        				},{
	        					xtype:'combo',
	        					reference:'ctlDeliveryMode',
	        					fieldLabel: me.lblDeliveryMode,
	        					labelWidth: 120,
	                            width:280,
	                            align : 'left',
	                            bind: {
	            	    			store: '{delvModeCombo}'
	            	    		},
	            	    		displayField: 'name',
	           					valueField: 'code',
	           					queryMode: 'local',
	           					value : ''
		                      },{

		    					reference: 'ctlFromDt',
		    					xtype: 'datefield',
		    					width: 200,
		    					fieldLabel: me.lblEta,
		    					format: MOST.config.Locale.getShortDate(),
		    					editable: true,
		    					listeners: {
		    						change: 'onDateChange'
		    					}
		    				  
		                      },{
		                    	reference: 'ctlToDt',
		    			        xtype: 'datefield',
		    			        width: 120,
		    			        format: MOST.config.Locale.getShortDate(),
		    			        editable: true,
		    					listeners: {
		    						change: 'onDateChange'
		    					}
		                      }]
			            	}
			            ]
					}]
				},
				
				
				
				{
				xtype: 'grid',
				reference: 'refListOfDeliveryOrderGrid',
				flex : 1,
				stateful : true,
				stateId : 'stateListOfDeliveryOrderGrid',
				viewConfig: {
		            stripeRows: true,
		            enableTextSelection: true,
		        },
				plugins: [
					'gridexporter',
					'gridfilters',
					'clipboard'
	    		],
	    		bind: {
	    			store: '{listOfDeliveryOrder}'
	    		},
	    		selModel: {
					type: 'spreadsheet',
					cellSelect: false
				},
				listeners: {
					cellDblClick: 'onDblClick'
				},
				// =========== columns ===========
				// =========== columns ===========
				// =========== columns ===========
				columns: {
	            	defaults: {
	            		style : 'text-align:center',
	            		align : 'center'
	            	},
	            	items: [
	            		{
		            		xtype: 'rownumberer',
		            		width : 40,
		            		locked : true,
		            		align : 'center'
		    	        },
	            		{
	            			header: me.lblBlno,
	            			dataIndex: 'blno',
	            			reference: 'refBlno',
	            			filter : 'string',
	            			align : 'center',
	            			locked : true,
	            			width: 150
	            		},          		
	            		{
	            			header: me.lblDeliveryMode,
	            			dataIndex: 'delvTpNm',
	            			reference: 'refDeliveryMode',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblDoNo,
	            			dataIndex: 'dono',
	            			reference: 'refDoNo',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblJpvc,
	            			dataIndex: 'vslCallId',
	            			reference: 'refVslCallId',
	            			filter : 'string',
	            			align : 'left',
	            			width: 150
	            		},
	            		{
	            			header: me.lblVslNm,
	            			dataIndex: 'vslNm',
	            			reference: 'refVslNm',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblGp,
	            			dataIndex: 'gpYn',
	            			reference: 'refGp',
	            			filter : 'string',
	            			align : 'center',
	            			width: 80
	            		},
	            		{
	            			header: me.lblAcknowledge,
	            			dataIndex: 'ackby',
	            			reference: 'refAcknowledge',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblAcknowledgeDt,
	            			dataIndex: 'ackdt',
	            			reference: 'refAcknowledgeDt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 170
	            		},
	            		{
	            			header: me.lblModeofOperation,
	            			dataIndex: 'btspttpcdnm',
	            			reference: 'refModeofOperation',
	            			filter : 'string',
	            			align : 'center',
	            			width: 130
	            		},
	            		{
	            			header: me.lblTransporter,
	            			dataIndex: 'tsptr',
	            			reference: 'refTransporter',
	            			filter : 'string',
	            			align : 'center',
	            			width: 90
	            		},
	            		{
	            			header: me.lblDocMt,
	            			dataIndex: 'wgt',
	            			reference: 'refDocMt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblDocM3,
	            			dataIndex: 'vol',
	            			reference: 'refDocM3',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblDocQty,
	            			dataIndex: 'pkgqty',
	            			reference: 'refDocQty',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblDirectMt,
	            			dataIndex: 'dmt',
	            			reference: 'refDirectMt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblDirectM3,
	            			dataIndex: 'dm3',
	            			reference: 'refDirectM3',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblDirectQty,
	            			dataIndex: 'dqty',
	            			reference: 'refDirectQty',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblInDirectMt,
	            			dataIndex: 'imt',
	            			reference: 'refInDirectMt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblInDirectM3,
	            			dataIndex: 'im3',
	            			reference: 'refInDirectM3',
	            			filter : 'string',
	            			align : 'center',
	            			width: 110
	            		},
	            		{
	            			header: me.lblInDirectQty,
	            			dataIndex: 'iqty',
	            			reference: 'refInDirectQty',
	            			filter : 'string',
	            			align : 'center',
	            			width: 110
	            		},
	            		{
	            			header: me.lblNumOfPkg,
	            			dataIndex: 'pkgqty',
	            			reference: 'refNumOfPkg',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblTypeOfPackage,
	            			dataIndex: 'pkgtpcd',
	            			reference: 'refTypeOfPackage',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblIssueDt,
	            			dataIndex: 'issuedt',
	            			reference: 'refIssueDt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblConsignee,
	            			dataIndex: 'cnsneecd',
	            			reference: 'refConsignee',
	            			filter : 'string',
	            			align : 'center',
	            			width: 100
	            		},
	            		{
	            			header: me.lblFwdAgent,
	            			dataIndex: 'ptnrcd',
	            			reference: 'refFwdAgent',
	            			filter : 'string',
	            			align : 'center',
	            			width: 70
	            		},
	            		{
	            			header: me.lblCargoTp,
	            			dataIndex: 'cgTpCdNm',
	            			reference: 'refCargoTp',
	            			filter : 'string',
	            			align : 'center',
	            			width: 130
	            		},
	            		{
	            			header: me.lblDgClass,
	            			dataIndex: 'imdgunno',
	            			reference: 'refDgClass',
	            			filter : 'string',
	            			align : 'center',
	            			width: 130
	            		},
	            		{
	            			header: me.lblCustDeclNo,
	            			dataIndex: 'releaseNo',
	            			reference: 'refCustDeclNo',
	            			filter : 'string',
	            			align : 'center',
	            			width: 165
	            		},
	            		{
	            			header: me.lblCustApprStat,
	            			dataIndex: 'customsAprvStat',
	            			reference: 'refCustApprStat',
	            			filter : 'string',
	            			align : 'center',
	            			width: 200
	            		},
	            		{
	            			header: me.lblCustApprDt,
	            			dataIndex: 'customsAprvDt',
	            			reference: 'refCustApprDt',
	            			filter : 'string',
	            			align : 'center',
	            			width: 200
	            		},
	            		{
	            			header: me.lblGdsRmk,
	            			dataIndex: 'gdsrmk',
	            			reference: 'refGdsRmk',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblCommodity,
	            			dataIndex: 'cmdtcd',
	            			reference: 'refCommodity',
	            			filter : 'string',
	            			align : 'center',
	            			width: 150
	            		},
	            		{
	            			header: me.lblRmk,
	            			dataIndex: 'domark',
	            			reference: 'refRmk',
	            			filter : 'string',
	            			align : 'center',
	            			width: 90
	            		}]
				}
		    }],
		    
		    dockedItems: [
		    	{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align:'left'
					},
					defaults: {
						margin: '1 1 1 1'
					},
					items: [
					{
						xtype: 'button',
						itemId: 'inquiryItemId',
    					text: me.btnRetrieve,
    					reference: 'refBtnRetrieve',
    					iconCls: 'x-fa fa-search',
    					cls: 'search-button',
    					listeners: {
    						click: 'onSearch'
    					}
					}, {
						xtype: 'button',
						itemId: 'deleteItemId',
						reference:'refBtnDelete',
						text: me.btnDelete,
						ui: 'delete-button',
						iconCls: 'x-fa fa-minus',
						listeners: {
							click: 'onRemove'
						}
					},{
						xtype: 'button',
						itemId: 'previewItemId',
						text: me.btnPreviewToPDF,
						reference:'refBtnDownload',
						cls: 'preview-button',
    					iconCls: 'fa fa-file-pdf-o',
						listeners: {
							click: 'onDetailPreview'
						}
					},
					{
						xtype: 'button',
						itemId: 'downloadItemId',
						reference:'refBtnPreview',
						text: me.btnDownload,
    					cls: 'downloadpdf-button',
    					iconCls: 'fa fa-file-excel-o',
						listeners: {
							click: 'onDetailDownload'
						},
					},{
	    				xtype :'container',
	    				layout:{
	    					type : 'hbox',
	    					pack : 'end'
	    				},
	    				flex : 1,
	    				margin : '0 0 0 0',
	    				items:[
	    					{
	    		                ui: 'default-toolbar',
	    		                xtype: 'button',
	    		                margin : '1 5 0 0',
	    		                cls: 'dock-tab-btn',
	    		                text: 'Export to ...',
	    		                menu: {
	    		                    defaults: {
	    		                        handler: 'exportTo'
	    		                    },
	    		                    items: [{
	    		                        text: 'Excel xlsx',
	    		                        cfg: {
	    		                            type: 'excel07',
	    		                            ext: 'xlsx'
	    		                        }
	    		                    }, {
	    		                        text: 'Excel xlsx (include groups)',
	    		                        cfg: {
	    		                            type: 'excel07',
	    		                            ext: 'xlsx',
	    		                            includeGroups: true,
	    		                            includeSummary: true
	    		                        }
	    		                    }, {
	    		                        text: 'Excel xml',
	    		                        cfg: {
	    		                            type: 'excel03',
	    		                            ext: 'xml'
	    		                        }
	    		                    }, {
	    		                        text: 'Excel xml (include groups)',
	    		                        cfg: {
	    		                            includeGroups: true,
	    		                            includeSummary: true
	    		                        }
	    		                    }, {
	    		                        text: 'CSV',
	    		                        cfg: {
	    		                            type: 'csv'
	    		                        }
	    		                    }, {
	    		                        text: 'TSV',
	    		                        cfg: {
	    		                            type: 'tsv',
	    		                            ext: 'csv'
	    		                        }
	    		                    }, {
	    		                        text: 'HTML',
	    		                        cfg: {
	    		                            type: 'html'
	    		                        }
	    		                    }, {
	    		                        text: 'HTML (include groups)',
	    		                        cfg: {
	    		                            type: 'html',
	    		                            includeGroups: true,
	    		                            includeSummary: true
	    		                        }
	    		                    }]
	    		                }
	    		            }		
	    				]
	    			}]
				}]			
		});
		
		me.callParent();
	}
});

