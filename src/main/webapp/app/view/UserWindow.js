Ext.define('sencha.view.UserWindow', {  //define UserWindow
    extend: 'Ext.window.Window',  //extend wimdow class
    alias: 'widget.userWindow',//define alias
    
    requires: [
    	'sencha.controller.TabController', 
    	'sencha.view.UserGridView'
    ],//requirement of class that use in this page
    
    controller: 'tabController',//use controller
    
    title: 'User Details',
    autoScroll:true,
   
    height: 400,
    width: 600,
    id: 'popWindow',
    itemID: 'popWindow',
    
    items: [{
        xtype: 'userGridView',  //use grid view
    }]

});

