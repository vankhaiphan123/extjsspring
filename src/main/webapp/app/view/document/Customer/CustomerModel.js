Ext.define('sencha.view.document.CustomerModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.customer',

	requires: [
		'Ext.data.proxy.Rest',
		'sencha.model.document.Customer'
	],
	
	stores: {
		listOfCustomer: {
			model: 'sencha.model.document.Customer',
			storeId: 'listOfCustomerStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/customer/list'
			}
		},
		
		customerDetail: {
			model: 'sencha.model.document.Customer',
			storeId: 'customerDetail',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/customer/detail'
			}
		},
		
		cgOpList: {
			model: 'MOST.model.popup.CargoMaster',
			storeId: 'cgOpListStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/customer/cgOpList'
			}
		},
		
		directTstpCombo : {
			fields: ['scdNm','scd'],
			storeId: 'directTstpCombo',
			proxy: {
			   type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/commonCode/codeMasterList'
			},
			listeners: 
			{
				load: function(store, records) {
			          store.insert(0, [{
			        	  scdNm: 'Select',
			              scd: ''
			          }]);
			     }
			}
		},
		
//		indirectTstpCombo : {
//			fields: ['scdNm','scd'],
//			storeId: 'indirectTstpComboStore',
//		},
		
		delvModeCombo : {
			fields: ['name','code'],
			storeId: 'delvModeComboStore',
			data :  [{"code":"", "name":"Select"},
					{"code":"B", "name":"Both Direct/Indirect"},
					{"code":"I", "name":"Indirect"}
            ]
		},
		
		commonCodePopup: {
			model: 'MOST.model.popup.CMMCdPopup',
			storeId: 'commonCodePopupStore',
			proxy: {
				type: 'rest',
				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/commonCode/commonCodeList'
			}
		},
		
//		generatePDFDeliveryOrder: {
//			model: 'MOST.model.document.DeliveryOrder',
//			storeId: 'generatePDFDeliveryOrderStore',
//			proxy: {
//				type: 'rest',
//				url: MOST.config.Locale.getRestApiDestUrl() + '/v1/customer/previewpdfdo'
//			}
//		},
	}
	
});